
import datetime
import json
import argparse
import configparser
import pymysql
import sendgridendesa as sgend
# before install dependencies with : pip install -r requierements.txt
# VALOR PARAMETROS
parser = argparse.ArgumentParser(description='Descripción de parámetros de entrada', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('filepath', help='Configuration file', type=str)
args = vars(parser.parse_args())
filename = args.get('filepath')
if args.get('filepath'):

    configParser = configparser.RawConfigParser()
    configParser.read(filename)

    # valor del properties mysql
    configBDPro1 = configParser.get('configBDPro', 'local')
    configBDPro2 = configParser.get('configBDPro', 'usuario')
    configBDPro3 = configParser.get('configBDPro', 'permiso')
    configBDPro4 = configParser.get('configBDPro', 'base_datos')
    #configBDPre = {
    #    'host': '130.211.93.67',
    #    'user': 'pie',
    #    'passwd': 'Galeon123',
    #    'db': 'gestionfirma'
    #}
    #DATOS DE CORREO
    from_email = configParser.get('sendgrid', 'FROM_EMAIL')
    sendgrid_key = configParser.get('sendgrid', 'APIKEY')
    TO_EMAIL = configParser.get('notification-emails', 'to_email')

    # TODAS LAS OPERACIONES FIRMADAS SIN ENVIAR (status 1)
    status1 = "state = 1 AND"
    # TODAS LAS OPERACIONES FIRMADAS Y ENVIADAS AYER (status 2)
    status2 = "state = 2 AND"
    # TODAS LAS OPERACIONES CREADAS AYER
    status3 = ""

    query = ("SELECT * FROM gestionfirma.operation WHERE {status} creationDate LIKE '%{day_date}%'; ")
    query2 = ("SELECT * FROM gestionfirma.operation WHERE state = 2 and creationDate LIKE '%{day_date}%';")


    # this function return the searched to the execution a query in mysql
    def run_query(query):

        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "), "INFO ", " CONSULTA REALIZADA", file=open("output.txt", "a"))

        # Connect
        cnx = pymysql.connect(configBDPro1, configBDPro2, configBDPro3, configBDPro4)
        cursor = cnx.cursor()
        # Execute SQL select statement
        cursor.execute(query)
        # Save all datas inside array data
        data = []
        data.append(cursor.fetchone())
        while data[-1] is not None:
            data.append(cursor.fetchone())
        # Close the connection
        cursor.close()
        cnx.close()
        # return all elements except the last-
        return data[:-1]


    # this function return the date to yesterday, except if the date today is monday that return saturday and friday
    def last_date():
        lastDay = []
        # Get the current day with format yyyy-mm-dd
        today = datetime.date.today()
        # If the day is Tuesday (1), Wednesday(2), Thursday(3), Friday(4) or saturday (5)=> Get the last day
        if (1 <= today.weekday() <= 5):
            lastDay.append(today - datetime.timedelta(days=1))
        # If the day is Monday (0) => Get the last Saturday and Friday
        if (today.weekday() == 0):
            lastDay.append(today - datetime.timedelta(days=2))
            lastDay.append(today - datetime.timedelta(days=3))
        if (today.weekday() == 6):
            print('Sorry, today is sunday')
        return lastDay

    # this function return un array con the date the x days except sundays
    def last_x_days(numDays):
        days = []
        # Get the current day with format yyyy-mm-dd
        today = datetime.date.today()
        for i in range(numDays):
            possibleDate = today - datetime.timedelta(i + 1)
            if (possibleDate.weekday() != 6):
                days.append(possibleDate)
        return days

    # this function return the operation state
    def stateOperation(operationHoy):
        states = {
            1: 'FIRMA, PERO NO ENVIADA',
            2: 'ENVIADA',
        }
        return states.get(operationHoy, "UNDEFINED")

    #**********************METODO DE INFORME **********************************************************************************

    days = last_date() # informe de un solo dia
    #days = last_x_days(3)   #solucion para cuando hay fiestas o quieres hacer un informe de mas dias

    #********************************************************************************************************

    # operationsPeerDay = {}
    numdays = ""
    for i in days:
        numdays = numdays + i.strftime(' %Y-%m-%d')

    operationsPeerDay = ""
    result_FIRMADAS_SIN_ENVIAR_AYER = []
    result_FIRMADAS_ENVIADAS_AYER = []
    result_CREADAS_AYER = []
    for i in range(len(days)):
        query_FIRMADAS_SIN_ENVIAR_AYER = query.format(status=status1, day_date=days[i].strftime('%Y-%m-%d'))
        query_FIRMADAS_ENVIADAS_AYER = query.format(status=status2, day_date=days[i].strftime('%Y-%m-%d'))
        query_CREADAS_AYER = query.format(status=status3, day_date=days[i].strftime('%Y-%m-%d'))
        result_FIRMADAS_SIN_ENVIAR_AYER = run_query(query_FIRMADAS_SIN_ENVIAR_AYER)
        result_FIRMADAS_ENVIADAS_AYER = run_query(query_FIRMADAS_ENVIADAS_AYER)
        result_CREADAS_AYER = run_query(query_CREADAS_AYER)

        # result = []
        # result.append(len(result_FIRMADAS_SIN_ENVIAR_AYER))
        # result.append(len(result_FIRMADAS_ENVIADAS_AYER))
        # result.append(len(result_CREADAS_AYER))
        # operationsPeerDay[days[i].strftime('%Y-%m-%d')] = result

        operationsPeerDay = operationsPeerDay + ('''<p> Buenos dias ,
                                                    <p>Producción de endesa ha pasado los test rutinarios esta mañana.
                                                    <br>
                                                    <p>El {lastDay} se crearon {operaciones_creadas} operaciones de las cuales se firmaron y 
                                                      enviaron un total de {operaciones_enviadas}, quedando firmadas sin enviar {operaciones_sinEnviar}.
                                                   </p>''').format(lastDay=days[i].strftime('%Y-%m-%d'),
                                                                   operaciones_creadas=len(result_CREADAS_AYER),
                                                                   operaciones_enviadas=len(result_FIRMADAS_ENVIADAS_AYER),
                                                                   operaciones_sinEnviar=len(
                                                                       result_FIRMADAS_SIN_ENVIAR_AYER))

    query_FIRMADAS_ENVIADAS_HOY = query2.format(day_date=datetime.date.today().strftime('%Y-%m-%d'))
    result_FIRMADAS_ENVIADAS_HOY = []
    result_FIRMADAS_ENVIADAS_HOY = run_query(query_FIRMADAS_ENVIADAS_HOY)

    # resultToday = []
    # resultToday.append(len(result_FIRMADAS_ENVIADAS_HOY))
    # operationsPeerDay[datetime.date.today().strftime('%Y-%m-%d')] = resultToday


    operationsPeerDay = operationsPeerDay + ('''<p>A d&iacute;a de hoy, {fecha}, ya se han firmado y enviado {operaciones_creadas} operaciones.
                                                <br>
                                                <p>Cordiales saludos. 
                                                </p>''').format(operaciones_creadas=len(result_FIRMADAS_ENVIADAS_HOY),
                                                                fecha=datetime.date.today().strftime('%Y-%m-%d'))

    # print collections.OrderedDict(sorted(operationsPeerDay.items()))

    tableOperations = ""
    showNumOperations = 3
    for x in range(showNumOperations):
        operationHoy = result_FIRMADAS_ENVIADAS_HOY[x]
        # encoded
        dataUser = json.loads(operationHoy[4])

        tableOperations = tableOperations + ('''
                                        <tr>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{id}</td>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{idOperation}</td>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{creationDate}</td>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{nif}</td>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{name} </td>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{phone}</td>
                                            <td style=" border: 1px solid #dddddd; text-align: center; padding: 8px;">{state}</td>
                                        </tr>
                                        ''').format(id=operationHoy[0], idOperation=operationHoy[1],
                                                    creationDate=operationHoy[2].strftime('%Y-%m-%d %H:%M:%S'),
                                                    nif=dataUser['nif'], name=dataUser['nombre'].encode('utf-8'),
                                                    phone=dataUser['telefono'], state=stateOperation(operationHoy[5]))

    #creacion del title del mensaje
    subject = "Operaciones [ EndesaBio]"+numdays+" "
    #creacion del mensaje "body"
    cuerpo_correo = ('''<html> 
                            <head>
                                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                            </head>
                            <body  width="80%" style="margin: 0; font-family: sans-serif;"  text= "#424242" > 
                            <br>
                            {ini}
                            <br>
                            <table style="width:80%; font-size: 13px; line-height: 20px; ">
                                 <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #AED6F1;" COLSPAN="7" > Operaciones hechas en ENDESA: </td> 
                             <tr>   
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> ID </td> 
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> idOperation</td> 
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> CreationDate </td> 
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> NIF </td> 
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> Nombre </td> 
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> Teléfono </td> 
                                <td style=" border: 2px solid #a7b6d6; text-align: center; padding: 8px; background-color: #CAC0C0; "> State</td> 
                                
                                 {fin}
                            </table>
                            </body>                     
                        </html>  ''').format(fin=tableOperations,ini=operationsPeerDay)

#lista de destinatarios
to_email_arr = TO_EMAIL.split(";")
for to_emailf in to_email_arr:
    #llamamos a la funcion correo
    sgend.send_email(to_emailf, subject, cuerpo_correo, sendgrid_key, from_email, 'text/html')